# Project makefile for blamecount
#
# Requires Python 3 and asciidoctor. Tesrts also want pylint and shellcheck.

VERSION=$(shell sed -n <NEWS.adoc '/::/s/^\([0-9][^:]*\).*/\1/p' | head -1)

SOURCES = README.adoc COPYING NEWS.adoc blamecount blamecount.adoc blamecount.1 Makefile \
	control

.SUFFIXES: .html .adoc .1

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

clean:
	rm -f ascii ascii.o splashscreen.h nametable.h
	rm -f *.rpm *.tar.gz MANIFEST *.1 *.html

prefix?=/usr/local
mandir?=share/man
target=$(DESTDIR)$(prefix)

install: blamecount.1
	install -d "$(target)/bin"
	install -d "$(target)/$(mandir)/man1"
	install -m 755 blamecount "$(target)/bin"
	install -m 644 blamecount.1 "$(target)/$(mandir)/man1"
	if [ -O blamecount.1 ]; then rm blamecount.1; fi

uninstall:
	-rm "$(target)/$(mandir)/man1/blamecount.1"
	-rm "$(target)/bin/blamecount"
	-rmdir -p "$(target)/$(mandir)/man1"
	-rmdir -p "$(target)/bin"

reflow:
	@black -q blamecount

pylint:
	@pylint --score=n blamecount

shellcheck:
	@-shellcheck -s sh -f gcc bctest

check: pylint shellcheck
	@./bctest | ./tapview

# Don't do pylint on Gitlab, there's a version-skew problem.
cicheck: shellcheck
	@./bctest | ./tapview

version:
	@echo $(VERSION)

blamecount-$(VERSION).tar.gz: $(SOURCES)
	@ls $(SOURCES) | sed s:^:blamecount-$(VERSION)/: >MANIFEST
	@(cd ..; ln -s blamecount blamecount-$(VERSION))
	(cd ..; tar -czf blamecount/blamecount-$(VERSION).tar.gz `cat blamecount/MANIFEST`)
	@ls -l blamecount-$(VERSION).tar.gz
	@(cd ..; rm blamecount-$(VERSION))

dist: blamecount-$(VERSION).tar.gz

release: blamecount-$(VERSION).tar.gz blamecount.html
	shipper version=$(VERSION) | sh -e -x

refresh: blamecount.html
	shipper -N -w version=$(VERSION) | sh -e -x
